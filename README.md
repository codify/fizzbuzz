# FizzBuzz

FizzBuzz is a simple program that does the following

- Print "Fizz" instead of the number if
  - Number is divisible by 3 OR
  - Number includes 3

- Prints "Buzz" instead of the number if
    - Number is divisible by 5 OR
    - Number includes 5

- For numbers which are multiples of both three and five OR the numbers that include both three and five print "FizzBuzz"

### Prerequisites

Homebrew. Refer to http://brew.sh/ for how to.


### Installing

There is a setup script that installs rbenv to set the correct ruby version and associated gems

`bin/setup`

If you prefer not to use rbenv

- Set the ruby version to 2.3.1
- `gem install bundler`
- `bundle install`

## Running the tests

Just run `rspec` from the project directory to run all the tests

## Running for the first 100 numbers

`ruby fizz_buzz_caller.rb`
