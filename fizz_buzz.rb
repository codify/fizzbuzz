def fizz_buzz(number)
  result = ""
  result = "Fizz" if fizz?(number)
  result += "Buzz" if buzz?(number)
  result = number if result.empty?
  result
end

def fizz?(number)
  divisible_by_three?(number) || includes_three?(number)
end

def buzz?(number)
  divisible_by_five?(number) || includes_five?(number)
end

def divisible_by_three?(number)
  number % 3 == 0
end

def divisible_by_five?(number)
  number % 5 == 0
end

def includes_three?(number)
  number.to_s.include? "3"
end

def includes_five?(number)
  number.to_s.include? "5"
end
