require_relative "../fizz_buzz"

describe "#fizz_buzz" do
  context "number is only divisible by 3" do
    it "returns Fizz" do
      expect(fizz_buzz(3)).to eq "Fizz"
    end
  end

  context "number is only divisible by 5" do
    it "returns Buzz" do
      expect(fizz_buzz(5)).to eq "Buzz"
    end
  end

  context "number is divisible by 3 and 5" do
    it "returns FizzBuzz" do
      expect(fizz_buzz(15)).to eq "FizzBuzz"
    end
  end

  context "number is not divisible by 3 or 5" do
    context "number includes 3" do
      it "returns Fizz" do
        expect(fizz_buzz(13)).to eq "Fizz"
      end
    end

    context "number includes 5" do
      it "returns Buzz" do
        expect(fizz_buzz(58)).to eq "Buzz"
      end
    end

    context "number includes 3 and 5" do
      it "returns FizzBuzz" do
        expect(fizz_buzz(53)).to eq "FizzBuzz"
      end
    end

    context "number does not include 3 or 5" do
      it "returns the number" do
        expect(fizz_buzz(1)).to eq 1
      end
    end
  end
end
